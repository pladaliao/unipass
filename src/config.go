package main

import (
	"fmt"
	"encoding/json"
	"io/ioutil"
	"sync"
)

type UnipassConfig struct {
	ListeningPort    string    `json:"listen"`
	DBConfig DBConfig `json:"db"`
}

type DBConfig struct {
	Host   string `json:"host"`
	Port   string `json:"port"`
	Name   string `json:"name"`
	User   string `json:"user"`
	Pass   string `json:"pass"`
}

var singleton *UnipassConfig
var once sync.Once

func getConfig() *UnipassConfig {
	once.Do(func() {
		jsonFile, err := ioutil.ReadFile("config/default.conf")
		if err != nil {
			fmt.Println("read config error: %v", err)
		}
		json.Unmarshal(jsonFile, &singleton)
		if err != nil {
			fmt.Println("config parsing error: %v", err)
		}
    })
    return singleton
}